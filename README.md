Enowa Projekt für Straßenbegleitgrün. Siehe dazu UML Diagramm

N-tier Spring Boot Projekt, Postgres Datenbank, Spring Data JPA als DB API, Restful API

Schreiben Sie ein Java-Programm, welches die entsprechenden Entitäten via JPA in einer
PostgreSQL-Datenbank verwaltet.
Implementieren Sie eine Funktion welche die Datenbank mit Dummy-Testdaten füttert.
Schreiben Sie einen Webservice, über den eine Frontend-Anwendung alle Bäume einer Strasse
und der ggf. vorhandenen Befunde abgerufen kann:

- Die Frontend-Anwendung soll alle Bäume einer Straße als Punkte auf einer Karte zeigen
  und benötigt daher die Geokoordinaten der Bäume.
- Das Frontend soll die Baume in der Karte je nach Befunddaten einfärben und benötigt
  daher die Liste der Befunde
- Das Frontend soll per Pop-up die Details wie Spezies und Alter eines Baumes anzeigen
  und benötigt daher Zugriff auf diese Informationen



![UML](Analyse_Model.png)

