package com.enowa.pflanzenliebe;


import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class PflanzenliebeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PflanzenliebeApplication.class, args);
    }

    /**
     * Ich habe mich für eine openSource mapper lib entschieden.
     * Bei vielen Feldern und Abhänigkeiten erspart mir diese library einiges an Schreibarbeit
     * und sorgt für eine größere Übersicht.
     * @return ModelMapper bean
     */
    @Bean
    public ModelMapper getModelmapper(){
        return new ModelMapper();
    }


}
