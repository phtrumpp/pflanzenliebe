package com.enowa.pflanzenliebe.services;

import com.enowa.pflanzenliebe.dto.StadtModel;
import java.util.List;

public interface StadtService {


    Long saveOrUpdate(StadtModel stadtModel);

    StadtModel findById(Long id);

    List<StadtModel> findAll();



}
