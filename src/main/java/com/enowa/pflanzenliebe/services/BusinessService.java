package com.enowa.pflanzenliebe.services;

import com.enowa.pflanzenliebe.dto.*;


import java.util.List;

public interface BusinessService {


    Long save(BaumModel baumModel);

    BaumModel findBaumById(Long id);

    List<BaumModel> findAll();

    List<BefundModel> getBefundListe(BaumModel baumModel);

    List<GeoLocationModel> getLocation(List<BaumModel> baumListe);

    List<BaumModel> findAll(StrassenModel strasse);

    List<FrontendResponse> getJoinInformation();






}
