package com.enowa.pflanzenliebe.services.impl;

import com.enowa.pflanzenliebe.TestDummyGenerator;
import com.enowa.pflanzenliebe.repositories.*;
import com.enowa.pflanzenliebe.services.TestDummyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestDummyServiceImpl implements TestDummyService {

    private final BaumRepo baumRepo;
    private final BefundRepo befundRepo;
    private final GeoLocationRepo geoLocationRepo;
    private final SpeziesRepo speziesRepo;
    private final StadtRepo stadtRepo;
    private final StrasseRepo strasseRepo;

    @Autowired
    public TestDummyServiceImpl(BaumRepo baumRepo, BefundRepo befundRepo, GeoLocationRepo geoLocationRepo, SpeziesRepo speziesRepo, StadtRepo stadtRepo, StrasseRepo strasseRepo) {
        this.baumRepo = baumRepo;
        this.befundRepo = befundRepo;
        this.geoLocationRepo = geoLocationRepo;
        this.speziesRepo = speziesRepo;
        this.stadtRepo = stadtRepo;
        this.strasseRepo = strasseRepo;
    }


    @Override
    public void createTestDummies() {

        TestDummyGenerator generator = new TestDummyGenerator();


        stadtRepo.saveAll(generator.getStaedte());
        strasseRepo.saveAll(generator.getStrassen());
        speziesRepo.saveAll(generator.getSpezies());
        geoLocationRepo.saveAll(generator.getLocations());
        baumRepo.saveAll(generator.getBaeume());
        befundRepo.saveAll(generator.getBefuende());
    }
}
