package com.enowa.pflanzenliebe.services.impl;

import com.enowa.pflanzenliebe.dto.StadtModel;
import com.enowa.pflanzenliebe.entities.Stadt;
import com.enowa.pflanzenliebe.repositories.StadtRepo;
import com.enowa.pflanzenliebe.services.StadtService;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

// ------------- Für die Aufgabe vrmtl. nicht benötigt ----------------- //

@Service
@Transactional
public class StadtServiceImpl implements StadtService {



    private final StadtRepo repo;
    private final ModelMapper modelMapper;


    @Autowired
    public StadtServiceImpl(StadtRepo repo, ModelMapper modelMapper) {
        this.repo = repo;
        this.modelMapper = modelMapper;

        // Konfiguriere meinen mapper so dass er private Felder anspricht und nicht strikt ist.
        // Siehe dazu modelmapper doc
        this.modelMapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE) // gibt dem Mapper Zugriff auf private Felder
                .setMatchingStrategy(MatchingStrategies.LOOSE);
    }


    @Override
    public Long saveOrUpdate(StadtModel stadtModel) {

        Stadt entity = mapper(stadtModel);
        repo.save(entity);

        return entity.getStadtId();
    }



    @Override
    @Transactional
    public StadtModel findById(Long id) {
        //returns an optional
        Stadt stadt = repo.findById(id)
                .orElseThrow(() -> new IllegalStateException("Could not be found"));

        return mapper(stadt);
    }

    @Override
    public List<StadtModel> findAll() {

        List<Stadt> staedteListe = repo.findAll();
        List<StadtModel> stadtModelList = new ArrayList<>();

        for (Stadt stadt: staedteListe) {
            stadtModelList.add(mapper(stadt));
        }
        return stadtModelList;
    }

    // ------------------ Object Mapper ------------------- //

    /**
     * Benutzt einen opensource Modelmapper
     * @param stadtModel DTO als Argument
     * @return Stadt Entität
     */
    private Stadt mapper(StadtModel stadtModel) {
        return modelMapper.map(stadtModel,Stadt.class);
    }

    /**
     * Benutzt einen opensource Modelmapper
     * @param stadt Entität als Argument
     * @return Stadtmodel DTO
     */
    private StadtModel mapper(Stadt stadt) {
        return modelMapper.map(stadt,StadtModel.class);
    }


}
