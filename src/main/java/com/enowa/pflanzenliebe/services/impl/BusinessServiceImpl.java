package com.enowa.pflanzenliebe.services.impl;

import com.enowa.pflanzenliebe.TestDummyGenerator;
import com.enowa.pflanzenliebe.dto.*;
import com.enowa.pflanzenliebe.entities.Baum;
import com.enowa.pflanzenliebe.entities.Befund;
import com.enowa.pflanzenliebe.entities.GeoLocation;
import com.enowa.pflanzenliebe.repositories.BaumRepo;
import com.enowa.pflanzenliebe.repositories.BefundRepo;
import com.enowa.pflanzenliebe.repositories.GeoLocationRepo;
import com.enowa.pflanzenliebe.services.BusinessService;
import org.aspectj.weaver.ast.Test;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("BusinessService")
@Transactional
public class BusinessServiceImpl implements BusinessService {


    private final BaumRepo baumRepo;
    private final BefundRepo befundRepo;
    private final GeoLocationRepo geoLocationRepo;


    private final ModelMapper modelMapper; // mapper von DTO - Entität und andersrum


    @Autowired
    public BusinessServiceImpl(BefundRepo befundRepo, BaumRepo baumRepo, GeoLocationRepo geoLocationRepo, ModelMapper modelMapper) {

        this.befundRepo = befundRepo;
        this.baumRepo = baumRepo;
        this.geoLocationRepo = geoLocationRepo;
        this.modelMapper = modelMapper;

        // Siehe dazu modelmapper doc
        this.modelMapper.getConfiguration()
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE) // gibt dem Mapper Zugriff auf private Felder
                .setMatchingStrategy(MatchingStrategies.LOOSE);
    }


    @Override
    public Long save(BaumModel baumModel) {

        Baum baum = baumRepo.save(mapper(baumModel));

        return baum.getBaumId();
    }

    @Override
    public BaumModel findBaumById(Long id) {
        return mapper(baumRepo.findById(id)
                .orElseThrow(() -> new IllegalStateException("No entity found")));
    }

    @Override
    public List<BaumModel> findAll() {

        List<Baum> baumListe = baumRepo.findAll();
        List<BaumModel> baumModelList = new ArrayList<>();

        if (!baumListe.isEmpty()) {
            for (Baum baum : baumListe) {
                baumModelList.add(mapper(baum));
            }
        } else {
            throw new IllegalStateException("Baumliste is empty");
        }

        return baumModelList;
    }

    @Override
    public List<BaumModel> findAll(StrassenModel strasse) {

        List<Baum> baumListe = baumRepo.findAllByBaumStrasse(strasse);
        List<BaumModel> baumModelList = new ArrayList<>();

        if (!baumListe.isEmpty()) {
            for (Baum baum : baumListe) {
                baumModelList.add(mapper(baum));
            }
        } else {
            throw new IllegalStateException("Baumliste is empty");
        }

        return baumModelList;
    }

    @Override
    public List<FrontendResponse> getJoinInformation() {
        return baumRepo.getJoinInfo();
    }



    /**
     * Für den schnellen Zugriff auf die Befundsliste eines Baumes.
     *
     * @param baumModel übergib einen RequestBody
     * @return Liste von Befund DTOs
     */
    @Override
    public List<BefundModel> getBefundListe(BaumModel baumModel) {

        List<Befund> befundList = befundRepo.getBefundListe(mapper(baumModel).getBaumId())
                .orElseThrow(() -> new IllegalStateException("No 'Befund' found")); // öffnen aus dem Optional wrapper

        List<BefundModel> befundModelList = new ArrayList<>();

        for (Befund befund : befundList) {
            befundModelList.add(mapper(befund));
        }

        return befundModelList;
    }

    /**
     * Das Frontend schickt von einer Straße die dazugehörige Liste an Bäumen.
     * Diese Liste wird dann für den schnellen Zugriff auf die Koordinaten eines jeweiligen Baumes verwendet
     *
     * @return Koordinaten eines Baumes
     */
    @Override
    public List<GeoLocationModel> getLocation(List<BaumModel> baumListe) {

        List<GeoLocationModel> locationList = new ArrayList<>();
        for (BaumModel baum : baumListe) {

            Optional<GeoLocation> geoLocationByBaum = geoLocationRepo.getGeoLocationByBaum(mapper(baum));

            locationList.add(mapper(geoLocationByBaum
                    .orElseThrow(() -> new IllegalStateException("No location found"))));
        }


        return locationList;
    }


    // ----------------- Object Mapper ----------------------------------------------- //
    // ------------------------------------------------------------------------------- //


    private Baum mapper(BaumModel baumModel) {
        return modelMapper
                .map(baumModel, Baum.class);
    }

    private BaumModel mapper(Baum baum) {
        return modelMapper
                .map(baum, BaumModel.class);
    }

    private BefundModel mapper(Befund befund) {
        return modelMapper
                .map(befund, BefundModel.class);
    }

    private GeoLocationModel mapper(GeoLocation location) {
        return modelMapper
                .map(location, GeoLocationModel.class);
    }


}
