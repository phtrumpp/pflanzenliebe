package com.enowa.pflanzenliebe.controller;

import com.enowa.pflanzenliebe.dto.*;
import com.enowa.pflanzenliebe.services.BusinessService;
import com.enowa.pflanzenliebe.services.impl.TestDummyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class FrontendController {

    private final BusinessService businessService;

    private final TestDummyServiceImpl testDummyService;

    @Autowired
    public FrontendController(BusinessService businessService, TestDummyServiceImpl testDummyService) {
        this.businessService = businessService;
        this.testDummyService = testDummyService;
    }


    /**
     *
     * Die Frontend-Anwendung soll alle Bäume einer Straße als Punkte auf einer Karte zeigen
     * und benötigt daher die Geokoordinaten der Bäume.
     *
     * Siehe GeoLocationRepo
     *
     * @return Liste von Bäumen mit ihren Koordinaten
     */
    @GetMapping("/location")
    public List<GeoLocationModel> getLocationInfo(@RequestBody StrassenModel strassenModel){

        return businessService.getLocation(strassenModel.getBaeume());

    }


    /**
     * Das Frontend soll die Baume in der Karte je nach Befunddaten einfärben und benötigt
     * daher die Liste der Befunde
     *
     * siehe BefundRepo für die verwendete Query
     *
     * @param baumModel Frontend übergibt die BaumId
     * @return die Befundsliste des Baums
     */
    @GetMapping("/befundsliste")
    public List<BefundModel> getBefundsListe(@RequestBody BaumModel baumModel){
        return businessService.getBefundListe(baumModel);
    }


    /**
     * "Das Frontend soll per Pop-up die Details wie Spezies und Alter der Bäume anzeigen
     *  und benötigt daher Zugriff auf diese Informationen"
     *
     * Custom Query mit JOIN, um Name und Alter (pflanzDatum) der Bäume zu bekommen
     * siehe dazu Baumrepo
     *
     * @return Liste von DTOs mit pflanzDatum
     */
    @GetMapping("/joininfo")
    public List<FrontendResponse> getJoinInformation(){
        return businessService.getJoinInformation();
    }


    // TodO - Methode noch nicht verfeinert genug
    @GetMapping("/baeume")
    public List<BaumModel> getBaeumeInfo(@RequestBody StrassenModel strasse){
        return businessService.findAll(strasse);
    }


    /**
     * Testdaten Generator mit der Mockneat Library
     * @return Statuscode als 200 bei Erfolg
     */

    @GetMapping("/testdata")
    public String createTestData(){
        testDummyService.createTestDummies();
        return "200 - überprüfe deine Datenbank";
    }




}
