package com.enowa.pflanzenliebe.repositories;




import com.enowa.pflanzenliebe.entities.Befund;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface BefundRepo extends JpaRepository<Befund,Long> {

    /**
     * Das Frontend soll die Baume in der Karte je nach Befunddaten einfärben und benötigt
     * daher die Liste der Befunde
     * @param baumId jeder Befund ist an einem Baum und seiner ID gebunden.
     * @return Befundsliste eines Baumes
     */
    @Query("select b.befuende from Baum b where b.baumId = :baumId")
    Optional<List<Befund>> getBefundListe(@Param("baumId") Long baumId);

}
