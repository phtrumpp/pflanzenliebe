package com.enowa.pflanzenliebe.repositories;


import com.enowa.pflanzenliebe.entities.Baum;
import com.enowa.pflanzenliebe.entities.GeoLocation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GeoLocationRepo extends CrudRepository<GeoLocation,Long> {


    /**
     * Die Frontend-Anwendung soll alle Bäume einer Straße als Punkte auf einer Karte zeigen
     * und benötigt daher die Geokoordinaten der Bäume
     *
     * Mit Verwendung des Spring Data JPA Signaturen query
     *
     * @param baum
     * @return Koordinaten des Baumes
     */
    Optional<GeoLocation> getGeoLocationByBaum(Baum baum);
}
