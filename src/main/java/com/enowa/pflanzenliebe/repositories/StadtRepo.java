package com.enowa.pflanzenliebe.repositories;

import com.enowa.pflanzenliebe.entities.Stadt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;




@Repository
public interface StadtRepo extends JpaRepository<Stadt,Long> {
}
