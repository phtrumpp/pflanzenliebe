package com.enowa.pflanzenliebe.repositories;


import com.enowa.pflanzenliebe.entities.Spezies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpeziesRepo extends JpaRepository<Spezies,Long> {


}
