package com.enowa.pflanzenliebe.repositories;

import com.enowa.pflanzenliebe.entities.Strasse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StrasseRepo extends CrudRepository<Strasse,Long> {
}
