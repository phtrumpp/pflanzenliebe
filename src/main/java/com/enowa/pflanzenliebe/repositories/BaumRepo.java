package com.enowa.pflanzenliebe.repositories;

import com.enowa.pflanzenliebe.dto.FrontendResponse;
import com.enowa.pflanzenliebe.dto.StrassenModel;
import com.enowa.pflanzenliebe.entities.Baum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BaumRepo extends JpaRepository<Baum,Long> {

    List<Baum> findAllByBaumStrasse(StrassenModel strasse);


    /**
     *   "Das Frontend soll per Pop-up die Details wie Spezies und Alter eines Baumes anzeigen
     *    und benötigt daher Zugriff auf diese Informationen"
     *
     * @return eine Liste von DTOs mit der gewünschten Info
     */
                    // Joins die DTOs zurückgeben müssen mit package Namen angegeben werden
    @Query("select new com.enowa.pflanzenliebe.dto.FrontendResponse(b.pflanzdatum, s.botanischerName) from Baum b Join b.baumSpezies s")
    List<FrontendResponse> getJoinInfo();


}
