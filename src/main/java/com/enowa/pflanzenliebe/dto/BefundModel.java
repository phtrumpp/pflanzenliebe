package com.enowa.pflanzenliebe.dto;


import lombok.Data;

import java.util.Date;

@Data
public class BefundModel {

    public BefundModel() {
    }

    private Date erhobenAm;
    private String beschreibung;

    private BaumModel meinBaum;


}
