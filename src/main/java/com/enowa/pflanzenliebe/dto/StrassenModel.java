package com.enowa.pflanzenliebe.dto;

import com.enowa.pflanzenliebe.entities.Baum;

import lombok.Data;

import java.util.List;

@Data
public class StrassenModel {

    private Long id;

    private String name;
    private String verwaltungsKuerzel;

    private List<BaumModel> baeume;

    private StadtModel stadt;

    public StrassenModel() {
    }

    // Für Testzwecke
    public StrassenModel(String name, String verwaltungsKuerzel) {
        this.name = name;
        this.verwaltungsKuerzel = verwaltungsKuerzel;
    }


}
