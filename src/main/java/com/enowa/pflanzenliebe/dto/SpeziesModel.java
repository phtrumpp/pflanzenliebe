package com.enowa.pflanzenliebe.dto;



import lombok.Data;

import java.util.List;

@Data
public class SpeziesModel {

    public SpeziesModel() {
    }

    private String botanischerName;
    private String trivialName;

    private List<BaumModel> baumListe;
}
