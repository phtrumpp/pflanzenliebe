package com.enowa.pflanzenliebe.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;


@Data
@NoArgsConstructor
@ToString
public class FrontendResponse {

    private Date date;
    private String botanischerName;

    public FrontendResponse(Date date, String botanischerName) {
        this.date = date;
        this.botanischerName = botanischerName;
    }
}
