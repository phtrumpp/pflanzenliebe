package com.enowa.pflanzenliebe.dto;


import lombok.Data;

@Data
public class GeoLocationModel {

    public GeoLocationModel() {
    }

    private double latitude;
    private double longitude;

    private BaumModel baum;
}
