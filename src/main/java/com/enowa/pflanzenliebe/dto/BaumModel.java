package com.enowa.pflanzenliebe.dto;

import com.enowa.pflanzenliebe.entities.Befund;
import com.enowa.pflanzenliebe.entities.GeoLocation;
import com.enowa.pflanzenliebe.entities.Spezies;
import com.enowa.pflanzenliebe.entities.Strasse;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BaumModel {

    public BaumModel() {
    }

    private Long id;

    private long nummer;
    private Date pflanzdatum;

    private StrassenModel baumStrasse;

    private List<BefundModel> befuende;

    private SpeziesModel baumSpezies;

    private GeoLocationModel location;
}
