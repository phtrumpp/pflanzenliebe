package com.enowa.pflanzenliebe.dto;


import lombok.Data;

import java.util.List;

@Data
public class StadtModel {

    public StadtModel() {
    }

    private Long id;

    private String plz;
    private String name;

    private List<StrassenModel> strassen;


}
