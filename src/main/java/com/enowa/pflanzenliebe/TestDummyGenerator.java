package com.enowa.pflanzenliebe;


import com.enowa.pflanzenliebe.entities.*;
import lombok.Data;
import net.andreinc.mockneat.MockNeat;

import java.util.List;

import static net.andreinc.mockneat.unit.time.LocalDates.localDates;
import static net.andreinc.mockneat.unit.types.Doubles.doubles;
import static net.andreinc.mockneat.unit.types.Longs.longs;


// Todo - Beinhaltet noch kleine Logikfehler...

@Data
public class TestDummyGenerator {


    /*  Mockneat Library um zufällige Testdaten zu erzeugen, allerdings erhalte ich noch IllegalstateExceptions
        außerdem bin ich noch nicht drauf gekommen wie ich ganze Listen in die Objekte mocke.

        Die Funktion wird über den Restcontroller mit einem Get Request und einem extra Service aufgerufen.
        Es sollen nur die Entitäten gespeichert werden.

        doc dazu findet man auf https://www.mockneat.com/docs/


     */

    private MockNeat mock = MockNeat.old();


    private final List<Stadt> staedte = mock.reflect(Stadt.class)
            .field("stadtId", mock.longSeq())
            .field("name",mock.cities().capitalsEurope())
            .field("plz",mock.regex("Group [A-Z]{3}[0-9]{2}"))
            .list(10)
            .val();

    private final List<Strasse> strassen = mock.reflect(Strasse.class)
            .field("strassenId",mock.longSeq().start(100).increment(100))
            .field("name",mock.regex("Group [A-Z]{3}[0-9]{2}"))
            .field("stadt",mock.from(staedte).map(Stadt::getStadtId).list(200))
            .list(2000)
            .val();

    private final List<Spezies> spezies = mock.reflect(Spezies.class)
            .field("botanischerName",mock.names().firstAndFemale())
            .field("trivialName",mock.names().last())
            .field("speziesId",mock.longSeq().start(100).increment(100))
            .list(250)
            .val();

    private final List<GeoLocation> locations = mock.reflect(GeoLocation.class)
            .field("locationId",mock.longSeq())
            .field("latitude",doubles().bound(999))
            .field("longitude",doubles().bound(999))
            .list(1600000)
            .val();


    private final List<Baum> baeume = mock.reflect(Baum.class)
            .field("baumId",mock.longSeq().start(100).increment(100))
            .field("nummer",longs().bound(801))
            .field("pflanzdatum",localDates().toUtilDate())
            .field("baumSpezies", mock.from(spezies).map(Spezies::getSpeziesId))
            .field("location",mock.from(locations).map(GeoLocation::getLocationId))
            .list(1600000)
            .val();


    private final List<Befund> befuende = mock.reflect(Befund.class)
            .field("befundId",mock.longSeq().start(100).increment(100))
            .field("erhobenAm",mock.localDates().toUtilDate())
            .field("beschreibung",mock.filler(()->"Filler Beschreibung"))
            .field("meinBaum", mock.from(baeume).map(Baum::getBaumId).list(5))
            .list(5)
            .val();



}
