package com.enowa.pflanzenliebe.entities;



import lombok.Data;
import javax.persistence.*;
import java.util.List;


@Entity
@Data
public class Strasse {


    private String name;
    private String verwaltungsKuerzel;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long strassenId;

    // 1 Strasse hat n Bäume
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Baum> baeume;

    // n Strassen haben 1 Stadt
    @ManyToOne
    @JoinColumn(name = "stadt_id")
    private Stadt stadt;

    public Strasse() {
    }

    public Strasse(String name, String verwaltungsKuerzel) {
        this.name = name;
        this.verwaltungsKuerzel = verwaltungsKuerzel;
    }

    public Strasse(String name, String verwaltungsKuerzel, Stadt stadt) {
        this(name, verwaltungsKuerzel);
        this.stadt = stadt;
    }

    /**
     * Das Hinzufügen und Entfernen einer Entität von der Beziehung erfordert das updaten von beiden Seiten.
     * @param baum
     */
    public void addBaum(Baum baum){
        this.baeume.add(baum);
        baum.setBaumStrasse(this);
    }

    public void remove(Baum baum){
        this.baeume.remove(baum);
        baum.setBaumStrasse(null);
    }

}
