package com.enowa.pflanzenliebe.entities;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Spezies {

    private String botanischerName;
    @Column(unique = true)
    private String trivialName;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long speziesId;

    // n Bäume gehören zu einer Spezies
    @OneToMany
    private List<Baum> baumListe;


    public Spezies() {
    }

    public Spezies(String botanischerName, String trivialName) {
        this.botanischerName = botanischerName;
        this.trivialName = trivialName;
    }

    public Spezies(String botanischerName, String trivialName, List<Baum> baumListe) {
        this(botanischerName,trivialName);
        this.baumListe = baumListe;
    }

    public void addBaum(Baum baum){
        this.baumListe.add(baum);
        baum.setBaumSpezies(this);
    }

    public void removeBaum(Baum baum){
        this.baumListe.remove(baum);
        baum.setBaumSpezies(null);
    }
}
