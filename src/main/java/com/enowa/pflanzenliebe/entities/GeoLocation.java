package com.enowa.pflanzenliebe.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class GeoLocation {


    private double latitude;
    private double longitude;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long locationId;

    // 1 Baum hat 1 Location
    @OneToOne
    private Baum baum;

    public GeoLocation() {
    }

    public GeoLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public GeoLocation(double latitude, double longitude, Baum baum) {
        this(latitude, longitude);
        this.baum = baum;
    }


    public void changeBaum(Baum baum){
        this.setBaum(baum);
        baum.setLocation(this);
    }
}
