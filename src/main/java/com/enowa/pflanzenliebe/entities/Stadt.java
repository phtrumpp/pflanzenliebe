package com.enowa.pflanzenliebe.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
public class Stadt {

    private String plz;
    private String name;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long stadtId;

    // 1 Stadt hat n Strassen
    @OneToMany(cascade = CascadeType.PERSIST)// CascadeType.ALL lässt alle Objekte
    // und Beziehungen innerhalb dieser Liste persistieren
    private List<Strasse> strassen;

    public Stadt() {
    }

    // Custom e.g. for lambda mapping
    public Stadt(String plz, String name) {
        this.plz = plz;
        this.name = name;
    }

    public Stadt(String plz, String name, List<Strasse> strassen) {
        this(plz, name);
        this.strassen = strassen;
    }

    /**
     * Das Hinzufügen und Entfernen einer Entität von der Beziehung erfordert das updaten von beiden Seiten.
     * @param strasse
     */
    public void addStrasse(Strasse strasse){
        this.strassen.add(strasse);
        strasse.setStadt(this);
    }

    // Assoziation verhindert das Entfernen einer Straße - eine Straße kann nicht ohne Stadt existieren

}
