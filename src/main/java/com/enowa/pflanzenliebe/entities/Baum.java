package com.enowa.pflanzenliebe.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class Baum {

    private long nummer;
    private Date pflanzdatum;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long baumId;

    // n Bäume haben 1 Strasse
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "strasse_id")
    private Strasse baumStrasse;

    // 1 Baum hat n Befünde
    @OneToMany(targetEntity = Befund.class,cascade = CascadeType.ALL) // Befünde können eh nicht existieren ohne Bäume
    private List<Befund> befuende;

    // n Bäume gehören zu einer Spezies
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "spezies_id")
    private Spezies baumSpezies;

    @OneToOne(targetEntity = GeoLocation.class,cascade = CascadeType.PERSIST)
    @JoinColumn(name = "location_id")
    private GeoLocation location;

    public Baum() {
    }

    // Custom for lambda mapping
    public Baum(long nummer, Date pflanzdatum) {
        this.nummer = nummer;
        this.pflanzdatum = pflanzdatum;
    }

    public Baum(long nummer, Date pflanzdatum,
                Strasse baumStrasse, List<Befund> befuende,
                Spezies baumSpezies, GeoLocation location) {

        this(nummer, pflanzdatum);
        this.baumStrasse = baumStrasse;
        this.befuende = befuende;
        this.baumSpezies = baumSpezies;
        this.location = location;
    }

    /**
     * Das Hinzufügen und Entfernen einer Entität von der Beziehung erfordert das updaten von beiden Seiten.
     * @param befund update Befund
     */
    public void addBefuende(Befund befund) {
        this.befuende.add(befund);
        befund.setMeinBaum(this);
    }
    // Eine remove Befund Methode ist bei einer Assoziation wie bei Befund zu Baum in dieser Klasse unmöglich

    /**
     * Das gleiche Prinzip für eine neue Straße
     * @param strasse
     */
    public void addStraße(Strasse strasse){
        this.setBaumStrasse(strasse);
        strasse.getBaeume().add(this);
    }

    public void removeStraße(){
        this.baumStrasse.getBaeume().remove(this);
        this.setBaumStrasse(null);
    }

    public void replaceSpezies(Spezies spezies){
        this.setBaumSpezies(spezies);
        spezies.getBaumListe().add(this);
    }


}
