package com.enowa.pflanzenliebe.entities;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class Befund {

    private Date erhobenAm;
    private String beschreibung;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long befundId;

    // n Befunde haben 1 Baum
    @ManyToOne
    @JoinColumn(name = "baum_id")
    private Baum meinBaum;

    public Befund() {
    }

    public Befund(Date erhobenAm, String beschreibung) {
        this.erhobenAm = erhobenAm;
        this.beschreibung = beschreibung;
    }

    public Befund(Date erhobenAm, String beschreibung, Baum meinBaum) {
        this(erhobenAm, beschreibung);
        this.meinBaum = meinBaum;
    }

    /**
     * Das Hinzufügen und Entfernen einer Entität von der Beziehung erfordert das updaten von beiden Seiten.
     * @param baum übergib einen RequestBody
     */
    public void updateBaum(Baum baum) {
        this.meinBaum.getBefuende().remove(this);
        this.setMeinBaum(baum);
    }




}
