package com.enowa.pflanzenliebe.services.impl;


import com.enowa.pflanzenliebe.dto.StadtModel;
import com.enowa.pflanzenliebe.dto.StrassenModel;
import com.enowa.pflanzenliebe.entities.Stadt;
import com.enowa.pflanzenliebe.entities.Strasse;
import com.enowa.pflanzenliebe.repositories.StadtRepo;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Disabled("Noch in Bearbeitung")
@RunWith(MockitoJUnitRunner.class)
class StadtServiceImplTest {


    @Mock
    private StadtRepo repo;
    @Mock
    private ModelMapper modelMapper;


//    @Autowired // ToDo - Autowired Alternative, da mocking momentan nicht funktioniert
    private StadtServiceImpl stadtService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        stadtService = new StadtServiceImpl(repo,modelMapper);
    }


    @Test
    void itShouldMapANDRead() {
        // Given
        List<Strasse> strassenListe = new ArrayList<>();


        Stadt testStadt = new Stadt();

        strassenListe.add(new Strasse("BreitenfelderStr. 19","Epp"));
        strassenListe.add(new Strasse("Höfendorfer Str. 33", "Hgl"));

        testStadt.setPlz("20251");
        testStadt.setName("Hamburg");
        testStadt.setStrassen(strassenListe);


        given(repo.save(testStadt));


        //then
        ArgumentCaptor<StadtModel> stadtArgumentCaptor =
                ArgumentCaptor.forClass(StadtModel.class);


        given(stadtService.findById(testStadt.getStadtId())).willReturn(stadtArgumentCaptor.capture());


        StadtModel expected = stadtArgumentCaptor.getValue();


        assertThat(expected)
                .isNotNull()
                .isInstanceOf(StadtModel.class);

//                .isEqualToComparingOnlyGivenFields(testStadt,"plz","name","strassen");


    }
}