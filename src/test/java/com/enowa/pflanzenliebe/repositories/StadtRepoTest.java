package com.enowa.pflanzenliebe.repositories;


import com.enowa.pflanzenliebe.entities.Stadt;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest(
        properties = {
                "spring.jpa.properties.javax.persistence.validation.mode=none"
        }
)
class StadtRepoTest {

    @Autowired
    private StadtRepo underTest;


    @Test
    void itShouldSave() {
        // Given
        Stadt stadt = new Stadt("72401","Haigerloch");

        // Then
        underTest.save(stadt);

        assertThat(underTest.findById(stadt.getStadtId()))
                .isPresent()
                .hasValueSatisfying(s -> assertThat(s).isEqualToComparingFieldByField(stadt));


    }



}